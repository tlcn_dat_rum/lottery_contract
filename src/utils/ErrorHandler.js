export default error => {
    if (!error.message) {
        return error;
    }
    if (error.message.includes("out of gas")) {
        return "Out of Gas";
    }
    if (error.message.includes("Owner doesn't have enough money")) {
        return "Owner doesn't have enough money to pay for you bet";
    }
    console.log(error);
    return "Unknown error";
};
