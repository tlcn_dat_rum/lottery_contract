import React, { Component, Fragment } from "react";

import InfoPanel from "./components/InfoPanel";
import BetPanel from "./components/BetPanel";
import AdminPanel from "./components/AdminPanel/index.jsx";
import { Route } from "react-router-dom";
import { connect } from "react-redux";
import { clearError } from "./actions/error";
import * as Types from "./actions/Types";
import LogPanel from "./components/LogPanel";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    async componentDidMount() {
        const contract = this.props.lottery.contract;
        const instance = await contract.deployed();
        this.props.deployedContract(instance);
    }

    render() {
        const { error, clearError } = this.props;

        return (
            <div className="container">
                <div className="page-header">
                    <h1>Lottery DApp</h1>
                </div>

                {error && (
                    <div className="row">
                        <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div className="alert alert-danger">
                                <button
                                    type="button"
                                    className="close"
                                    onClick={clearError}
                                >
                                    &times;
                                </button>
                                <strong>Error:</strong> {error}
                            </div>
                        </div>
                    </div>
                )}
                {this.props.lottery.instance && (
                    <Fragment>
                        <Route exact path="/" component={InfoPanel} />
                        <Route exact path="/" component={BetPanel} />
                        <Route exact path="/" component={LogPanel} />
                        <Route exact path="/admin" component={AdminPanel} />
                    </Fragment>
                )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        lottery: state.lottery,
        error: state.error
    };
}
function mapDispatchToProps(dispatch) {
    return {
        clearError: () => dispatch(clearError()),
        deployedContract: instance =>
            dispatch({ type: Types.DEPLOY_CONTRACT, payload: instance })
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
