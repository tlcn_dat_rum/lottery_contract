import toError from "../utils/ErrorHandler";
import * as Types from "./Types";

export const handleError = err => {
    return {
        type: Types.SET_ERROR,
        payload: toError(err)
    };
};

export const clearError = () => {
    return {
        type: Types.CLEAR_ERROR
    };
};
