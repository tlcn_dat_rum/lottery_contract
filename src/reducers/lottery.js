import Lottery from "../contracts/Lottery.json";
import * as Types from "../actions/Types";

import { NETWORK_URL } from "../config";

import truffleContract from "truffle-contract";
import Web3 from "web3";

const contract = truffleContract(Lottery);

if (typeof web3 !== "undefined") {
    console.log("Using web3 detected from external source like Metamask");
    this.web3 = new Web3(window.web3.currentProvider);
} else {
    console.log(
        "No web3 detected. Falling back to NETWORK_URL. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask"
    );
    this.web3 = new Web3(new Web3.providers.HttpProvider(NETWORK_URL));
}

contract.setProvider(this.web3.currentProvider);
if (typeof contract.currentProvider.sendAsync !== "function") {
    contract.currentProvider.sendAsync = function() {
        return contract.currentProvider.send.apply(
            contract.currentProvider,
            arguments
        );
    };
}

const initialState = {
    web3: this.web3,
    contract
};

export default (state = initialState, action) => {
    if (action.type === Types.DEPLOY_CONTRACT)
        return {
            ...state,
            instance: action.payload
        };
    return state;
};
