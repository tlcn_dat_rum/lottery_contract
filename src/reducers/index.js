import { combineReducers } from "redux";
import lottery from "./lottery";
import error from "./error";

export default combineReducers({
    error,
    lottery
});
