import * as Types from "../actions/Types";

const initialState = "";

export default function(state = initialState, action) {
    switch (action.type) {
        case Types.SET_ERROR:
            return action.payload;
        case Types.CLEAR_ERROR:
            return "";
        default:
            return state;
    }
}
