import React, { Component } from "react";
import { connect } from "react-redux";
import Pagination from "react-js-pagination";

class LogPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activePage: 0,
            totalPage: 10,
            rounds: [],
            logs: [],
            transactions: []
        };
    }

    async componentDidMount() {
        const { instance } = this.props.lottery;

        const StartGame = instance.StartGame({}, { fromBlock: "0" });

        StartGame.watch(async (err, result) => {
            if (err) {
                return this.props.handleError(err);
            }
            const { transactions } = this.state;
            const { transactionHash } = result;
            if (!transactions.includes(transactionHash)) {
                await this.setState({
                    transactions: [...transactions, result.transactionHash]
                });
                this.setState({
                    rounds: [...this.state.rounds, result.args]
                });
            }
        });
    }

    listenBetComplete = async (fromBlock, toBlock = "latest") => {
        const { instance } = this.props.lottery;
        if (typeof this.BetComplete !== "undefined") {
            this.BetComplete.stopWatching(err => {
                if (err) return console.log(err);
            });
        }
        this.BetComplete = instance.BetComplete(
            {},
            {
                fromBlock: fromBlock.toString(),
                toBlock: toBlock.toString()
            }
        );
        this.BetComplete.watch(async (err, result) => {
            if (err) {
                return this.props.handleError(err);
            }
            const { transactions } = this.state;
            const { transactionHash } = result;
            if (!transactions.includes(transactionHash)) {
                await this.setState({
                    transactions: [...transactions, result.transactionHash]
                });
                this.setState({
                    logs: [...this.state.logs, result.args]
                });
            }
        });
    };

    handlePageChange = pageNumber => {
        this.setState({
            activePage: pageNumber,
            logs: []
        });
        if (pageNumber >= this.state.rounds.length) {
            const startedBlock = this.state.rounds[pageNumber - 1].startedBlock;

            this.listenBetComplete(startedBlock);
        } else if (pageNumber > 0) {
            const startedBlock = this.state.rounds[pageNumber - 1].startedBlock;
            const endedBlock = this.state.rounds[pageNumber].startedBlock;

            this.listenBetComplete(startedBlock, endedBlock);
        }
    };

    render() {
        const { web3 } = this.props.lottery;
        const images = ["", "bau", "cua", "tom", "ca", "ga", "nai"];
        return (
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div className="panel panel-info">
                        <div className="panel-heading">
                            <h3 className="panel-title">History</h3>
                        </div>

                        {this.state.rounds.length > 0 && (
                            <div className="panel-body">
                                <div className="row">
                                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        {"Round: "}
                                    </div>
                                </div>

                                <div className="row text-center">
                                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <Pagination
                                            activePage={this.state.activePage}
                                            itemsCountPerPage={1}
                                            totalItemsCount={
                                                this.state.rounds.length
                                            }
                                            pageRangeDisplayed={5}
                                            onChange={this.handlePageChange}
                                        />
                                    </div>
                                </div>
                            </div>
                        )}

                        <ul className="list-group">
                            {this.state.logs.map((log, index) => {
                                return (
                                    <li key={index} className="list-group-item">
                                        <span className="label label-primary">
                                            {log.account}
                                        </span>
                                        {" bet "}
                                        <span className="label label-success">
                                            {web3.utils.fromWei(
                                                log.amount.toString()
                                            )}
                                            {" ETH"}
                                        </span>
                                        {" on "}
                                        <span>
                                            <img
                                                alt=""
                                                src={`/assets/images/${
                                                    images[
                                                        parseInt(
                                                            log.number.toString(),
                                                            10
                                                        )
                                                    ]
                                                }.png`}
                                                width="50"
                                                height="50"
                                            />
                                        </span>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { lottery: state.lottery };
}

export default connect(mapStateToProps)(LogPanel);
