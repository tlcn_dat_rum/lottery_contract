import React, { Component } from "react";
import className from "classnames";

class Number extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isHover: false
        };
    }

    onMouseEnter = () => {
        this.setState({ isHover: true });
    };

    onMouseLeave = () => {
        this.setState({ isHover: false });
    };

    render() {
        const { isHover } = this.state;
        const { amount, image } = this.props;

        return (
            <button
                type="button"
                className={className({
                    "bet-number": true,
                    btn: true,
                    "btn-danger": amount > 0,
                    "btn-primary": isHover,
                    "btn-default": !isHover
                })}
                onMouseEnter={this.onMouseEnter}
                onMouseLeave={this.onMouseLeave}
            >
                <img src={`/assets/images/${image}.png`} alt="" />
                {amount && <h4>{amount} ETH</h4>}
            </button>
        );
    }
}

export default Number;
