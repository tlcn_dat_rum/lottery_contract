import React, { Component } from "react";
import { connect } from "react-redux";

class AdminPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            adminAmount: 0,
            balance: 0
        };
    }

    componentWillMount() {
        this.updateState();
        const { instance } = this.props.lottery;
        instance.BalanceChanged({ fromBlock: "0" }).watch(() => {
            this.updateState();
        });
    }

    updateState = async () => {
        const { web3, instance } = this.props.lottery;

        Promise.all([web3.eth.getBalance(instance.address)])
            .then(result => {
                const [balance] = result;

                this.setState({
                    balance: web3.utils.fromWei(balance).toString()
                });
            })
            .catch(err => {
                console.log(err);
            });
    };

    onEndGame = async () => {
        const { web3, instance } = this.props.lottery;
        const accounts = await web3.eth.getAccounts();
        instance.endGame({ from: accounts[0] });
    };

    onDeposit = async e => {
        e.preventDefault();
        const { web3, instance } = this.props.lottery;
        const amount = this.state.adminAmount;

        try {
            const accounts = await web3.eth.getAccounts();
            const value = web3.utils.toWei(amount, "ether");
            const gas = "300000";
            await instance.deposit({
                from: accounts[0],
                value,
                gas
            });

            this.resetAmount();
        } catch (err) {
            console.log(err);
        }
    };

    onWithdraw = async e => {
        e.preventDefault();
        const { web3, instance } = this.props.lottery;
        const amount = this.state.adminAmount;

        try {
            const accounts = await web3.eth.getAccounts();
            const gas = await web3.eth.estimateGas({
                from: accounts[0],
                value: 0
            });

            await instance.withdraw(web3.utils.toWei(amount, "ether"), {
                from: accounts[0],
                value: 0,
                gas
            });

            this.resetAmount();
        } catch (err) {
            console.log(err);
        }
    };

    resetAmount = () => {
        this.setState({ adminAmount: 0 });
    };

    onHandleChange = e => {
        const target = e.target;

        const { name, value } = target;

        if (!value || /^0+$/.test(value)) {
            return this.setState({ [name]: 0 });
        }

        if (/^[0-9]+\.?[0-9]*$/.test(value)) {
            return this.setState({
                [name]: value.replace(/^0+([1-9])/, (match, number) => {
                    return number;
                })
            });
        }
    };

    render() {
        const { balance, adminAmount } = this.state;
        return (
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div className="panel panel-danger">
                        <div className="panel-heading">
                            <h3 className="panel-title">Admin panel</h3>
                        </div>

                        <div className="panel-body">
                            <form className="form-horizontal">
                                <div className="form-group">
                                    <label className="control-label col-sm-3">
                                        Balance:
                                    </label>

                                    <div className="col-sm-9">
                                        {balance} ETH
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label className="control-label col-sm-3">
                                        Deposit / Withdraw:
                                    </label>

                                    <div className="col-sm-9">
                                        <input
                                            className="form-control"
                                            name="adminAmount"
                                            value={adminAmount}
                                            onChange={this.onHandleChange}
                                        />
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="col-sm-9 col-sm-offset-3">
                                        <a
                                            className="btn btn-success"
                                            onClick={this.onDeposit}
                                        >
                                            Deposit
                                        </a>{" "}
                                        <a
                                            className="btn btn-primary"
                                            onClick={this.onWithdraw}
                                        >
                                            Withdraw
                                        </a>{" "}
                                        <a
                                            className="btn btn-danger"
                                            onClick={this.onEndGame}
                                        >
                                            EndGame
                                        </a>{" "}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { lottery: state.lottery };
}

export default connect(mapStateToProps)(AdminPanel);
