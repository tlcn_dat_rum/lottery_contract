import React, { Component } from "react";
import { connect } from "react-redux";
import { handleError } from "../../actions/error";

class InfoPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    async componentDidMount() {
        this.updateState();
        const { instance } = this.props.lottery;
        instance
            .BetComplete({
                fromBlock: "0"
            })
            .watch(() => {
                this.updateState();
            });
        instance
            .StartGame({
                fromBlock: "0"
            })
            .watch(() => {
                this.updateState();
            });
    }

    updateState = async () => {
        const { web3, instance } = this.props.lottery;
        const {
            minValueBet,
            numberOfBets,
            totalBet,
            lastWinningNumber1,
            lastWinningNumber2,
            lastWinningNumber3
        } = instance;

        Promise.all([
            minValueBet(),
            numberOfBets(),
            totalBet(),
            lastWinningNumber1(),
            lastWinningNumber2(),
            lastWinningNumber3()
        ])
            .then(result => {
                const [
                    minValueBet,
                    numberOfBets,
                    totalBet,
                    lastWinningNumber1,
                    lastWinningNumber2,
                    lastWinningNumber3
                ] = result;

                this.setState({
                    minValueBet: web3.utils.fromWei(
                        minValueBet.toString(),
                        "ether"
                    ),

                    numberOfBets: numberOfBets.toString(),
                    totalAmount: web3.utils.fromWei(
                        totalBet.toString(),
                        "ether"
                    ),
                    lastNumber1: lastWinningNumber1.toString(),
                    lastNumber2: lastWinningNumber2.toString(),
                    lastNumber3: lastWinningNumber3.toString()
                });
            })
            .catch(err => {
                console.log(err);
            });
    };

    render() {
        const {
            minValueBet,

            numberOfBets,
            totalAmount,
            lastNumber1,
            lastNumber2,
            lastNumber3
        } = this.state;

        console.log(this.state);

        const images = ["", "bau", "cua", "tom", "ca", "ga", "nai"];
        return (
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div className="panel panel-info">
                        <div className="panel-heading">
                            <h3 className="panel-title">Infomation</h3>
                        </div>

                        <div className="panel-body">
                            <div className="row">
                                <div className="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <b>Minimum Bet Value:</b>
                                </div>
                                <div className="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                                    {minValueBet} ETH
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <b>Number of Bets:</b>
                                </div>
                                <div className="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                                    <span className="badge badge-success">
                                        {numberOfBets}
                                    </span>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <b>Total Amount:</b>
                                </div>
                                <div className="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                                    {totalAmount} ETH
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                    <b>Last Winning Number:</b>
                                </div>
                                <div className="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                                    {lastNumber1 ? (
                                        <img
                                            alt=""
                                            src={`/assets/images/${
                                                images[lastNumber1]
                                            }.png`}
                                            width="50"
                                            height="50"
                                        />
                                    ) : null}{" "}
                                    {lastNumber2 ? (
                                        <img
                                            alt=""
                                            src={`/assets/images/${
                                                images[lastNumber2]
                                            }.png`}
                                            width="50"
                                            height="50"
                                        />
                                    ) : null}{" "}
                                    {lastNumber3 ? (
                                        <img
                                            alt=""
                                            src={`/assets/images/${
                                                images[lastNumber3]
                                            }.png`}
                                            width="50"
                                            height="50"
                                        />
                                    ) : null}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { lottery: state.lottery };
}
function mapDispatchToProps(dispatch) {
    return {
        handleError: err => dispatch(handleError(err))
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(InfoPanel);
