import React, { Component } from "react";
import Number from "../Number";
import { connect } from "react-redux";
import { handleError } from "../../actions/error";

class BetPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            betAmount: 0,
            selected: {},
            transaction: []
        };
    }

    async componentDidMount() {
        const { web3, instance } = this.props.lottery;
        const accounts = await web3.eth.getAccounts();
        const startedBlock = await instance.startedBlock();
        console.log(
            "Get current round, started at block:",
            startedBlock.toString()
        );
        this.listenBetComplete(startedBlock, accounts[0]);

        const StartGame = instance.StartGame({ fromBlock: "0" });

        StartGame.watch((err, result) => {
            if (err) {
                return this.props.handleError(err);
            }

            console.log(
                "Started round at block:",
                result.args.startedBlock.toString()
            );
            this.setState({ selected: {} });
            this.listenBetComplete(result.args.startedBlock, accounts[0]);
        });
    }
    componentWillUnmount() {
        this.StartGame.stopWatching();
    }

    listenBetComplete = async (startedBlock, account) => {
        const { instance } = this.props.lottery;

        if (typeof this.BetComplete !== "undefined") {
            this.BetComplete.stopWatching(err => {
                if (err) return console.log(err);
            });
        }

        this.BetComplete = instance.BetComplete(
            {},
            {
                fromBlock: startedBlock.toString(),
                address: account
            }
        );

        this.BetComplete.watch(async (err, result) => {
            if (err) {
                return this.props.handleError(err);
            }

            const { transaction } = this.state;
            const { transactionHash } = result;

            if (!transaction.includes(transactionHash)) {
                await this.setState({
                    transaction: [...transaction, result.transactionHash]
                });

                const number = result.args.number.toString();
                const amount = result.args.amount;

                console.log(
                    "Bet complete:",
                    amount.toString() + " on " + number
                );

                const selected = { ...this.state.selected };
                if (selected[number]) {
                    selected[number] += parseInt(amount, 10);
                } else {
                    selected[number] = parseInt(amount, 10);
                }

                this.setState({ selected });
            }
        });
    };

    onBetNumber = async number => {
        const { web3, instance } = this.props.lottery;

        const amount = this.state.betAmount;
        if (parseFloat(amount) <= 0) {
            return this.props.handleError("Invalid amount!");
        }
        try {
            const accounts = await web3.eth.getAccounts();

            const value = web3.utils.toWei(amount, "ether");
            const gas = "300000";
            await instance.bet(number, {
                from: accounts[0],
                value,
                gas
            });
            this.setState({ betAmount: 0 });
        } catch (err) {
            this.props.handleError(err);
        }
    };

    onHandleChange = e => {
        const target = e.target;

        const { name, value } = target;

        if (!value || /^0+$/.test(value)) {
            return this.setState({ [name]: 0 });
        }

        if (/^[0-9]+\.?[0-9]*$/.test(value)) {
            return this.setState({
                [name]: value.replace(/^0+([1-9])/, (match, number) => {
                    return number;
                })
            });
        }
    };

    render() {
        const { selected } = this.state;
        const { fromWei } = this.props.lottery.web3.utils;
        return (
            <div className="row">
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div className="panel panel-info">
                        <div className="panel-heading">
                            <h3 className="panel-title">Bet number</h3>
                        </div>
                        <div className="panel-body">
                            <form className="form-horizontal">
                                <div className="form-group">
                                    <label className="control-label col-sm-3">
                                        Amount:
                                    </label>

                                    <div className="col-sm-9">
                                        <input
                                            className="form-control"
                                            name="betAmount"
                                            value={this.state.betAmount}
                                            onChange={this.onHandleChange}
                                        />
                                    </div>
                                </div>
                            </form>

                            <div className="row">
                                <Numbers
                                    onBetNumber={this.onBetNumber}
                                    selected={selected}
                                    fromWei={fromWei}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const Numbers = props => {
    const { selected, fromWei } = props;

    const images = ["", "bau", "cua", "tom", "ca", "ga", "nai"];

    const elements = images.map((image, index) => {
        if (!image) return null;

        const value = index;
        const amount = selected[value.toString()]
            ? fromWei(selected[value.toString()].toString(), "ether")
            : "";

        return (
            <div
                key={index}
                className="col-xs-6 col-sm-4 col-md-2 col-lg-2"
                onClick={() => props.onBetNumber(value)}
            >
                <Number value={index} amount={amount} image={image} />
            </div>
        );
    });

    return elements;
};

function mapStateToProps(state) {
    return {
        lottery: state.lottery
    };
}
function mapDispatchToProps(dispatch) {
    return {
        handleError: err => dispatch(handleError(err))
    };
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(BetPanel);
