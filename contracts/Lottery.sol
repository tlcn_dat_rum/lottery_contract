pragma solidity ^0.4.24;

contract Lottery {

    address owner;
    uint public minValueBet; // minimum value of bet
    uint public totalBet; // total amount in round
    uint public numberOfBets; // number of player
    uint public maxNumberOfBets = 5; // default value 
    uint public lastWinningNumber1;
    uint public lastWinningNumber2;
    uint public lastWinningNumber3;
    uint public constant NUMBER_RANGE_MIN = 1;
    uint public constant NUMBER_RANGE_MAX = 6;
    uint public constant MAX_NUMBER_OF_BET = 100; // The maximum number of bet
    uint public startedBlock = 0;

    enum TransactionType {
        DEPOSIT,
        WITHDRAW,
        BET,
        PRIZE
    }

    event BetComplete(address account, uint amount, uint number);
    event StartGame(uint startedBlock);
    event BalanceChanged(TransactionType transactionType, uint changed);

    mapping(uint => BetInfo[]) betInfos;

    struct BetInfo{
        address account;
        uint amount;
    }

    modifier onlyOwner {
        require(owner == msg.sender, "Only owner can call this function");
        _;
    }

    constructor(uint _minValueBet, uint _maxNumberOfBets) public {
        owner = msg.sender;
        startedBlock = block.number;
        if(_minValueBet != 0){
            minValueBet = _minValueBet; 
        }
        if(_maxNumberOfBets > 0 && _maxNumberOfBets <= MAX_NUMBER_OF_BET){
            maxNumberOfBets = _maxNumberOfBets;
        }
    }

    function kill() public onlyOwner{
        endGame();
        selfdestruct(msg.sender);
    }

    function withdraw(uint amount) public onlyOwner {
        uint balance = address(this).balance;
        if(amount > 0){
            require(balance >= amount, "Invalid amount");
            owner.transfer(amount);
        }else{
            owner.transfer(balance);
        }
        emit BalanceChanged(TransactionType.WITHDRAW, amount);
    }

    function deposit() public payable{
        require(msg.value > 0,"Invalid amount");
        emit BalanceChanged(TransactionType.DEPOSIT, msg.value);
    }

    // Bet a number from 1 to 6
    function bet(uint numberSelected) public payable{
        require(numberSelected >= NUMBER_RANGE_MIN && numberSelected <= NUMBER_RANGE_MAX,"Please choose a number in range from 1 to 10");
        require(msg.value >= minValueBet,"Invalid amount");
        require((totalBet + msg.value) < (address(this).balance - totalBet),"Owner doesn't have enough money to pay for you bet");
        
        BetInfo memory info = BetInfo(msg.sender, msg.value);
        betInfos[numberSelected].push(info);
       
        numberOfBets++;
        totalBet += msg.value;
        
        emit BetComplete(msg.sender,msg.value,numberSelected);
        emit BalanceChanged(TransactionType.BET, msg.value);
    }

    function startGame() public {
        totalBet = 0;
        numberOfBets = 0;
        startedBlock = block.number;
        emit StartGame(block.number);
    }

    function endGame() public onlyOwner {
        uint sumAddress = 0;
        for(uint i = NUMBER_RANGE_MIN ; i <= NUMBER_RANGE_MAX ; i++){
            for(uint j = 0 ; j < betInfos[i].length ; j++){
                sumAddress += uint(betInfos[i][j].account) + betInfos[i][j].amount;
            }
        }

        uint winningNumber1 = generateRandomNumber(sumAddress + lastWinningNumber1);
        uint winningNumber2 = generateRandomNumber(sumAddress + lastWinningNumber2 + block.number);
        uint winningNumber3 = generateRandomNumber(sumAddress + lastWinningNumber3 + block.timestamp);
        transferPrizes(winningNumber1);
        transferPrizes(winningNumber2);
        transferPrizes(winningNumber3);

         // Delete playerIndex
        for(uint k = NUMBER_RANGE_MIN; k <= NUMBER_RANGE_MAX; k++){
            betInfos[k].length = 0;
        }

        lastWinningNumber1 = winningNumber1;
        lastWinningNumber2 = winningNumber2;
        lastWinningNumber3 = winningNumber3;
        

        emit BalanceChanged(TransactionType.PRIZE, 0);
        startGame();
    }

    function generateRandomNumber(uint mix) internal pure returns(uint) {
        uint numberGenerated = uint(sha256(abi.encodePacked(mix))) % NUMBER_RANGE_MAX + 1;
        return numberGenerated;
    }

    function transferPrizes(uint winningNumber) public {
        //uint totalPrizes = 0;
        if(betInfos[winningNumber].length > 0){
            uint numberOfWinner = betInfos[winningNumber].length;
            for(uint i = 0 ; i < numberOfWinner ; i++){
                betInfos[winningNumber][i].account.transfer(betInfos[winningNumber][i].amount * 2);
                //totalPrizes += betInfos[winningNumber][i].amount;
            }
        }
    }
} 