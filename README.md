# Ethereum Lotttery

# Overview

Ethereum Lotttery is a decentralize application based on Smart Contract in Ethereum Blockchain.

## Tools

-   Ubuntu `18.04.1 LTS`
-   NodeJs `v8.12.0`
-   NPM `6.4.1`
-   Visual Studio Code
-   Solidity `0.4.24`
-   Truffle `4.1.14`
-   Ganache CLI `6.1.8`
-   MetaMask `4.14.0`

## Installation

-   Install truffle framework `npm install -g truffle`
-   Install Ganache (EVM) `npm install -g ganache-cli`
-   Install dependences `npm install`
-   Modify contracts in folder `/contract`
-   Modify frontend in folder `/src`
-   Launch Ganache `npm start ganache`
-   Config network in `truffle.js` of `truffle-config.js` _(for Window)_
-   Config **HttpProvider** in `/src/App.js` if you don't use MetaMask
-   Deploy smart contract and run app `npm start`
