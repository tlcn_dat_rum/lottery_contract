const Lottery = artifacts.require("./Lottery.sol");

contract("Lottery", function(_valueBet,_maxNumberOfBets){
    let lotteryInstance;

    it("Initializes with valueBet and maxNumberOfBets", function(){
        return Lottery.deployed().then(function(instance){
            lotteryInstance = instance;
            return lotteryInstance.valueBet();
        }).then(function(valueBet){
            assert.equal(valueBet,1);
            return lotteryInstance.maxNumberOfBets();
        }).then(function(maxNumberOfBets){
            assert.equal(maxNumberOfBets,5);
        })
    })
})